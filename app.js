const Koa = require('koa'),
    Router = require('koa-router'),
    serve = require('koa-static'),
    path = require('path'),
    views = require('koa-views'),
    convert = require('koa-convert'),
    webpackMiddleware = require('koa-webpack-middleware'),
    webpackDevMiddleware = webpackMiddleware.devMiddleware,
    webpackHotMiddleware = webpackMiddleware.hotMiddleware,
    webpack = require('webpack'),
    webpackConf = require('./config/webpack.prod.config');

const PORT = process.env.PORT || 3000;

let app = new Koa(),
    router = new Router(),
    main = serve(path.join(__dirname, '/build/'));

router.param('id', (id, ctx, next) => {
        ctx.id = Number(id);
        if (isNaN(ctx.id)) {
            ctx.response.redirect('/error');
        }
        return next();
    })
    // router中嵌入有koa-static的，不能封装所有的静态资源请求，只能app中use
    .get('/:id', (ctx) => {
        ctx.response.body = { id: ctx.id };
    });

app.use(convert(webpackDevMiddleware(webpack(webpackConf), {
        watchOptions: {
            aggregateTimeout: 300,
            poll: true
        },
        reload: true,
        publicPath: webpackConf.output.publicPath,
        stats: {
            colors: true,
            chunks: false
        }
    })))
    .use(convert(webpackHotMiddleware(webpack(webpackConf))))
    .use(main)
    .use(router.routes())
    .use(router.allowedMethods());

const server = app.listen(PORT, '127.0.0.1', (err) => {
    if (err) {
        console.error(err)
        return
    }
    console.log(`HMR Listening at http://127.0.0.1:${PORT}`)
})
process.on('SIGTERM', () => {
    console.log('Stopping dev server')
    wdm.close()
    server.close(() => {
        process.exit(0)
    })
})


module.exports = app;