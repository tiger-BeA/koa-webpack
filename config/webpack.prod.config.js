const path = require('path'),
    webpack = require('webpack'),
    HtmlWebpackPlugin = require('html-webpack-plugin'),
    ExtracTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    devtool: 'eval-source-map',
    entry: [
        'babel-polyfill',
        'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=true',
        `${__dirname}/../app/index.js`
    ],
    output: {
        filename: '[name].bundle.js',
        path: `${__dirname}/build`
    },
    module: {
        rules: [{
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.css$/,
                use: ExtracTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader'],
                })
            },
            {
                test: /\.scss$/,
                // add：分离css和Js文件
                use: ExtracTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader', 'postcss-loader'],
                })
            },
            {
                test: /\.art$/,
                use: ['art-template-loader']
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: __dirname + "/../app/index.tpl.html"
        }),
        new ExtracTextPlugin('index.css'),
        // 热加载相关3插件
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        // “If you are using the CLI, the webpack process will not exit with an error code by enabling this plugin.”
        // https://github.com/webpack/docs/wiki/list-of-plugins#noerrorsplugin
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV)
            }
        })
    ],
    resolve: {
        extensions: ['.scss', '.js', '.art'],
        alias: {
            // box: './box.scss'
        } // 自定义路径别名
    },
    devServer: {
        hot: true,
        host: 'localhost',
        port: 3000,
        contentBase: path.resolve(__dirname, 'build')
    }
}