import testTpl from '../views/test'
import $ from 'jquery'
import './index'
import './box'

$('#root').html(testTpl({
    title: 'Elare'
}))

if (module.hot) {
    console.log('热渲染')
    module.hot.accept()
} else {
    console.log('不存在热渲染')
}